# Send color codes to all other ESP devices - fun classroom activity

import time
import machine
import util
import neopixel
from umqtt.robust import MQTTClient
from ubinascii import hexlify

np = neopixel.NeoPixel(machine.Pin(4), 1)

def sub_callback(topic, msg):

    message = msg.decode('utf-8')
    print((topic, msg))

    np[0] = util.getRGBFromColor(message)

    np.write()

def run():
    thing = util.Thing()
 
    client = MQTTClient(thing.id, thing.MQTTHost, thing.MQTTPort, thing.MQTTUser, thing.MQTTPassword)
    
    client.set_callback(sub_callback)
    client.connect()
    client.subscribe(topic, 0)

    button = machine.Pin(0)
    
    while True:
        if not button.value():  
            util.debounce(button)

            client.publish(thing.TopicColor, thing.color, 0)
            print('sent color', thing.color)

        time.sleep_ms(1)
        client.check_msg()
