# Used to turn Ikea lamp on and off

import time
import machine
from umqtt.robust import MQTTClient
from ubinascii import hexlify
import util

def run():

    # Get unique settings for this device like MQTT client id, host name
    thing = util.Thing()

    client = MQTTClient(thing.id, this.MQTTHost, thing.MQTTPort, thing.MQTTUser, thing.MQTTPassword)
    client.connect()

    button = machine.Pin(0)

    while True:
        if not button.value():
            # Software switch debounce  
            util.debounce(button)

            # Our Ikea lamp is listening for this - toggle light on if off, or off if on
            client.publish(thing.TopicSwitch, 'toggle', 0)
            print('sent command to light')

        time.sleep_ms(1)
