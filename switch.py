# Ikea lamp runs this code

rom umqtt.robust import MQTTClient
from machine import Pin
import util

def sub_callback(topic, msg):

    global pin2

    message = msg.decode('utf-8')
    print((topic, msg))

    if message == 'on':
        pin2.value(1)
    elif message == 'off':
        pin2.value(0)
    elif message == 'toggle':
        if pin2.value():
            pin2.value(0)
        else:
            pin2.value(1)
    else:
        print(message, 'what?')
        
def run():

    thing = util.Thing()
    
    global pin2

    pin2 = Pin(5, Pin.OUT)
    subclient = MQTTClient(thing.id, thing.MQTTHost, thing.MQTTPort, thing.MQTTUser, thing.MQTTPassword)

    subclient.set_callback(sub_callback)
    subclient.connect()
    subclient.subscribe(thing.TopicSwitch, 0)

    while True:
        print('wating for message...')
        subclient.wait_msg()

