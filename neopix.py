# Play with RGB LED

import machine 
import neopixel
import util

np = neopixel.NeoPixel(machine.Pin(4), 1)

# Designed to be used from the REPL prompt for class room to set colors
def set(red,green,blue):
    np[0] = util.getColor(red, green, blue)
        
    np.write()
